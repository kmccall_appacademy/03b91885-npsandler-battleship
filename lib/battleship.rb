require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Bob"), board = Board.new)
    @player = player
    @board = board
    @hit = false
  end

  def attack(target)
    board[target] = :x

    if board[target] == :self
      @hit = true
    else
      @hit = false
    end
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    target = player.get_play
    attack(target)

    display_status
  end

  def play
    play_turn until game_over?
  end

  def display_status
    board.display

    if @hit == true
      puts "It's a hit!"
      puts "There are #{count} ships remaining."
    else
      puts "It's a miss :("
    end

    @hit = false
  end
end

if $PROGRAM_NAME == __FILE__
  BattleshipGame.new.play
end
