

class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    puts "Where would you like to shoot?"
    move = gets.chomp.split(", ")
    move.map { |el| el.to_i }
  end
end
